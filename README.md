# Python for Fun

## Purpose of this Collection

Python for Fun turns 18 this year. From the first project "Lisp in Python" to
the current latest "Binary Trees and Functional Programming", the site is and
remains a collection of fairly small projects created mostly for fun. They are
aimed at the intermediate programmer; people who know Python and are fairly
comfortable with OOP and perhaps a bit of basic recursion. Most programs are
quite short, generally a few pages of code and all of the projects are
accompanied with a write-up.

Since 1995 I have found Python to be an excellent programming language, heads
above others I have used professionally and for fun. I've enjoyed watching
Python take a very clear lead in the last several years among programming
languages used in the Computer Science Dept as well as in the sciences
departments at my Alma Mater (University of Oregon).

I hope you enjoy these projects as much as I have. If you have comments or
suggestions for improvements You can email me at:

chris-meyers47 (atsiign) hotmail (dot) com

Many thanks to Paul Carduner and Jeff Elkner for their work on this page,
especially for Paul's graphic of "Psyltherin" (apologies to Harry Potter) and
to the teams behind reStructured text and Sphinx.

Chris Meyers - May 05, 2020
