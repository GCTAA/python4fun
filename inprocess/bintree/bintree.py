# bintree.py - binary trees from nested tuples

mytree = (50, (30, (20, (10, None, None), None), (40, None, None)),
      (80, (70, (60, None, None), None), (90, None, None)))

def ids(tree, sofar=[]) :
    if tree == None : return sofar
    (value,left,right) = tree
    return ids(left,sofar) + [id(tree)] + ids(right,sofar)
    
def values(tree) :
    if not tree : return []
    (value,left,right) = tree
    return values(left) + [value] + values(right)

def balance(tree) :
    return balance2(None, values(tree))

def balance2(btree, sorted) :
    if not sorted  : return btree
    split = len(sorted)/2
    btree = insert  (btree,sorted[split])   # midpoint
    btree = balance2(btree,sorted[0:split]) # left side
    btree = balance2(btree,sorted[split+1:])  # right side
    return btree

def insert(tree, newValue) :
    if not tree : return (newValue,None,None)
    (value,left,right) = tree
    if value == None     : return (newValue,None,None)
    if value == newValue : return tree  # already there
    elif newValue < value :
        return (value, insert(left,newValue), right)
    else :
        return (value, left, insert(right,newValue))

def contains(tree, target) :
    if tree == None : return False
    (value,left,right) = tree
    if  value == target : return True
    elif value > target : return contains(left,target)
    else                : return contains(right,target)

def delete(tree, target) :
    if tree == None : return None
    (value, left, right) = tree
    if target == value :  # delete this node
        if left and right :
            swp = minval(right)
            print "Swap val", swp
            return (swp, left, delete(right,swp))
        if left  == None : return right
        if right == None : return left
    elif target < value :
        return (value, delete(left,target), right)
    elif target > value :
        return (value, left, delete(right,target))
    print "???"
    
def minval(tree) :
    (val,left,right) = tree
    if left == None : return val
    else : return minval(left)

def printTree(tree, indent=" ") :
    (value,left,right) = tree
    if value != None :
        if left  : printTree(left,  indent+"  "); print indent+"|"
        print "%s ---%s" % (indent,value)
        if right : printTree(right, indent+"  "); print indent+"|"

toggle = {"   ": "  |", "  |": "   "}

def printTree(tree, indent=[]) :
    if tree == None : return
    (value,left,right) = tree
    if value != None :
        printTree(left,  indent+["   "])
        print "%s--%s" % ("".join(indent),value)
        if len(indent) : indent[-1] = toggle[indent[-1]]
        printTree(right, indent+["  |"])


