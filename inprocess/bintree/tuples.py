#!/usr/local/bin/python
#
# tuples.py - nodes of tuples to build linked list

sampleList = (10, (20, (30, (40, None))))

def showList(l, accum="") :
    if l == None : return accum
    else :
        if accum : sep = " --> "
        else     : sep = ""
        accum += sep + str(l[0])
        return showList(l[1], accum)

def contains(l, value) :
    if   l == None     : return False
    elif l[0] == value : return True
    else               : return contains(l[1],value)

def insert(l, value) :
    if   l == None    : return (value, None)
    elif l[0] > value : return (value, l)
    else              : return (l[0], insert(l[1], value))

def delete(l, value) :
    if   l    == None  : return None
    elif l[0] != value : return (l[0], delete(l[1],value))
    else               : return delete(l[1], value)

def main () :
    l = sampleList
    while True :
        print showList(l)
        val = raw_input("Input value to either insert or remove: ")
        if not val : break
        val = int(val)
        if contains(l, val) : l = delete(l, val)
        else                : l = insert(l, val)

if __name__ == "__main__" : main()
