# pgcon.py - connection into pygame

import time 

BGCOLOR = (150,150,150)    # was 3x75 - too dark
RED     = (255,  0,  0)
GREEN   = (  0,255,  0)
BLUE    = (  0,  0,255)
YELLOW  = (255,255,  0)
ORANGE  = (255,165,  0)
PURPLE  = (255,  0,255)
TURQUO  = (  0,255,255)
WHITE   = (255,255,255)
SIZE    = (600,600)

Monofont = "ubuntumono"

import pygame as pg

pg.init()

def setfont(name, size) :
    global fontsize, fontname, font
    if name == "mono": name = Monofont
    if name :
        fontname = pg.font.match_font(name)  
    else : fontname = None
    fontsize = size
    font     = pg.font.Font(fontname,fontsize)

#setfont ( sarg.Str("fontname",""), sarg.Int("fontsize",20))
setfont ( "", 20)

class Pgcon :
    def __init__ (self) :
        self.pg     = pg
        self.size   = (600,600)
        self.delay  = .5
        self.frame  = 0
        self.screen = pg.display.set_mode(self.size)

    def newScreen (self, background=BGCOLOR) :
        self.screen.fill(background)

    def lite(self, color) :
        # for example white (255,255,255) becomes gray (127,127,127)
        return [x/2 for x in color]
    
    def textDraw(self, color, pos, text,center=True) :
        xp,yp = pos
        for line in text.split("\n") :
            rend  = font.render(line,True,color)
            tw,th = font.size(line)

            if center : pos2 = (xp-tw/2, yp-th/2)
            else : pos2 = (xp,yp)
            self.screen.blit(rend, pos2)
            yp += th # pixel equivalent to font size
    
    def lineDraw(self,color,apos,bpos,width=1) :
        self.pg.draw.line(self.screen, color, apos, bpos, width)

    def writeScreen (self, delay=None) :
        self.pg.display.flip()
        #if delay == None : delay = self.delay
        #time.sleep(delay)
        #self.camera.takePicture(self.screen)
    
    def close (self, pause=True) :
        if pause : raw_input ("Hit Return to Continue")
        #self.camera.makeGif(delay=int(self.delay*100))
        self.pg.quit()
