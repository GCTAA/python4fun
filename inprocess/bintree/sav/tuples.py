# tuples.py - nodes of tuples to build linked list

sampleList = (10, (20, (30, (40, None))))

def showLinks(l) :
    if l != None :
        print "%d: %s %d" % (id(l), l[0], id(l[1]))
        showLinks(l[1])

def showList(l, accum="") :
    if l == None : return accum
    else :
        if accum : sep = " --> "
        else     : sep = ""
        accum += sep + "%d:%s" % (id(l)%10000,l[0]) # include last 4 dig of id
        return showList(l[1], accum)

def contains(l, value) :
    if   l == None     : return False
    elif l[0] == value : return True
    else               : return contains(l[1],value)

def insert(l, value) :
    if   l == None    : return (value, None)
    elif l[0] > value : return (value, l)
    else              : return (l[0], insert(l[1], value))

def delete(l, value) :
    if   l    == None  : return None
    elif l[0] == value : return l[1]
    else :
        l[1] = delete(l[1], value)
        return l

def testFunc (func) :
    print func
    l = sampleList
    while True:
        print l               # as python nested list
        print showList(l)     # human friendly
        v = raw_input("\ninput value: ")
        if not v : break
        l = func(l,int(v))
