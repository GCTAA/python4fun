# lists.py - nodes of 2 element lists to build linked list

sampleList = [10, [20, [30, [40, None]]]]

def showLinks(l) :
    if l != None :
        print "%d: %s %d" % (id(l), l[0], id(l[1]))
        showLinks(l[1])

def showList(l, accum="") :
    if l == None : return accum
    else :
        if accum : sep = " --> "
        else     : sep = ""
        accum += sep + "%d:%s" % (id(l)%10000,l[0]) # include last 4 dig of id
        return showList(l[1], accum)

def contains(l, value) :
    if   l == None     : return False
    elif l[0] == value : return True
    else               : return contains(l[1],value)

def insert1 (nod0, value) :
    if   nod0   == None  : return [value, None]
    elif nod0[0] > value : return [value, nod0[1]] # insert at begining
    else :
        nod = nod0
        while nod[1] :
            if nod[1][0] >= value  :   # next node in the chain?
                ins = (value, nod[1])  # new node built
                nod[1] = ins           # squeeze it in
                break
            else : nod = nod[1]
        if nod[1] == None : nod[1] = (value, None) # becomes the tail
        return nod0

def insert2(l, value) :
    if   l == None    : return [value, None]
    elif l[0] > value : return [value, l]
    else              : return [l[0], insert2(l[1], value)]

def delete1 (nod0, value) :
    if   nod0    == None  : return None
    elif nod0[0] == value : return nod0[1]  # if first node, just skip
    else :
        nod = nod0
        while nod[1] :
            if nod[1][0] == value  :  # next node in the chain?
                nod[1] = nod[1][1]    # skip the next
                break
            else : nod = nod[1]
        return nod0

def delete2(l, value) :
    if   l    == None  : return None
    elif l[0] == value : return l[1]
    else :
        l[1] = delete2(l[1], value)
        return l

def testFunc (func) :
    print func
    l = sampleList
    while True:
        print l               # as python nested list
        print showList(l)     # human friendly
        v = raw_input("\ninput value: ")
        if not v : break
        l = func(l,int(v))
