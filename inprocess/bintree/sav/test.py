#!/usr/local/bin/python
#
# create arc of a tree. Display it and then again balanced

from bintree     import balance, delete, ids, insert, contains, printTree
from displayTree import display, displayOff

def buildTree(sofar=None, values=[]) :
    if values :
       this = insert(sofar, values[0])
       return buildTree(this, values[1:]) 
    else : return sofar

def test() :
    bt = buildTree(values=[10,20,30,40,50,60,70,80,90])
    bt = balance(bt)
    print bt

    origIds = ids(bt)
    display(bt, (300,50), 300,50, origIds)
    while True :
        ans = raw_input("Type number or b or x: ")
        if   ans == 'b' : bt = balance(bt)
        elif ans == 'x' : break
        else :
            ans = int(ans)
            origIds = ids(bt)
            if contains(bt,ans) : bt = delete(bt,ans)
            else              : bt = insert(bt,ans)
        display(bt, (300,50), 300,50, origIds)
    displayOff() 

if __name__ == "__main__" : test()
