from pgcon import Pgcon, WHITE, BLUE, RED, YELLOW
pgcon  = Pgcon()

def display(tree, pos, width, stepdown, ids=None) :
    pgcon.newScreen()
    drawNode(tree, pos, width, stepdown, ids)
    pgcon.writeScreen(.500)

def displayOff() :
    pgcon.close()

def drawNode(node, pos, width, stepdown, ids) :
    if node == None : return
    x,y = pos; value,left,right = node
    halfwidth = width/2
    nextR = (x+halfwidth,y+stepdown)
    if left :
        nextL = (x-halfwidth,y+stepdown)
        pgcon.lineDraw(WHITE, pos, nextL, 1)
        drawNode(left , nextL, halfwidth, stepdown, ids)
    if right :
        nextR = (x+halfwidth,y+stepdown)
        pgcon.lineDraw(WHITE, pos, nextR, 1)
        drawNode(right, nextR, halfwidth, stepdown, ids)
    if ids and id(node) in ids : color = YELLOW
    else                       : color = RED
    pgcon.textDraw(color, pos, str(value))
