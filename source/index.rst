Python for Fun
==============

by Chris Meyers


Purpose of this Collection 
--------------------------

This collection is a presentation of several small Python programs. They are
aimed at intermediate programmers; people who have studied Python and are
fairly comfortable with basic recursion and object oriented techniques.  Each
program is very short, never more than a couple of pages and accompanied with a
write-up.

I have found Python to be an excellent language to express algorithms clearly.
Some of the ideas here originated in other programs in other languages. But in
most cases I developed code from scratch from just an outline of an idea.
However "Lisp in Python" was almost a translation exercise from John McCarthy's
original "Evalquote in Lisp".

From many years of programming these are some of my favorite programs. I hope
you enjoy them as much as I do. I look forward to hearing from readers,
especially folks with suggestions for improvements, ideas for new projects, or
people who are doing similar things. You can email me at mailme.html

Many thanks to Paul Carduner and Jeff Elkner for their work on this page,
especially for Paul's graphic of "Psyltherin" (apologies to Harry Potter) and
to the Sphinx developement team for their documentation generator to which all
the other web pages in this collection have been recently adapted.

.. image:: _static/pslytherin.png
   :class: pslytherin


The Collection
--------------

Text Processing
~~~~~~~~~~~~~~~

* `A Small, Simple and Fast Spelling Checker <spellcheck.html>`__

GUI Programming
~~~~~~~~~~~~~~~

* `Building a GUI with Tkinter <tkphone1.html>`__

Logic Circuits
~~~~~~~~~~~~~~

* `Simulating Logic Circuits with Python <logic.html>`__

Data Structures
~~~~~~~~~~~~~~~

* `Animal Tree of Knowlege <animal.html>`__


.. toctree::
   :maxdepth: 1
   :hidden:

   spellcheck.rst
   tkphone1.rst 
   logic.rst 
   mm_simulator.rst
   animal.rst


Reference
---------

* :ref:`genindex`
* :ref:`search`
