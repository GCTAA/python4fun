# bitmap.py - define the class bitmap


class Bitmap:
    def __init__(self, n_bits, word_size=16):
        self.n_bits = n_bits
        self.word_size = word_size
        self.n_words = (n_bits + self.word_size - 1) / self.word_size
        self.masks = []    # 2 ** n for n = 1..16 (bit to set or get)
        self.n_set = 0     # Bits actually set in the map

        # set masks to [1, 2, 4, 8, .., 32768] powers of 2
        bit = 1
        for i in range(self.word_size):
            self.masks.append(bit)
            bit <<= 1
        # create the empty bitmap
        self.map = [0] * self.n_words

    def _position(self, which):
        # from which bit number, calculate the mask and word address
        which_bit = which % self.n_bits
        self.which_word = which_bit / self.word_size
        self.mask = self.masks[which_bit % self.word_size]

    def set_bit(self, which):
        # set bit "which" to 1
        self._position(which)
        mask = self.mask
        if not self.map[self.which_word] & mask:
            self.n_set += 1                      # count bits actually set
            self.map[self.which_word] |= mask   # set bit to one

    def get_bit(self, which):
        # return True if bit 'which' is set, else False
        self._position(which)
        return (self.map[self.which_word] & self.mask) != 0


def test():
    # test code from the shell. python bitmap.py 5 16 17 5
    import sys
    tests = map(int, sys.argv[1:])
    bm = Bitmap(64)
    print bm.masks       # show the bit isolation masks
    for t in tests:
        print("Bit", t, bm.get_bit(t))
        if t >= 0:
            bm.set_bit(t)
        print(map(hex, bm.map))
        print("  now", bm.get_bit(t))


if __name__ == "__main__":
    test()
