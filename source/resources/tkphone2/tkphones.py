#!/usr/bin/env python3
import sqlite3
from tkinter import *

conn = sqlite3.connect('phones.db')
cur = conn.cursor()


def which_selected():
    print("At {0}".format(select.curselection()))
    return int(select.curselection()[0])


def dosql(cmd):
    print(cmd)
    cur.execute(cmd)
    set_select()


def add_entry():
    cur.execute("SELECT MAX(id)+1 FROM phones")
    nextid = cur.fetchone()[0]
    cmd = "INSERT INTO phones VALUES ({0}, '{1}', '{2}', '{3}')"
    dosql(cmd.format(nextid, fnamevar.get(), lnamevar.get(), phonevar.get()))


def update_entry():
    theid = phone_list[which_selected()][0]

    c = "UPDATE phones SET fname='{0}', lname='{1}', phone='{2}' WHERE id={3}"
    dosql(c.format(fnamevar.get(), lnamevar.get(), phonevar.get(), theid))


def delete_entry():
    theid = phone_list[which_selected()][0]
    dosql("DELETE FROM phones WHERE id={0}".format(theid))


def load_entry():
    thid, fname, lname, phone = phone_list[which_selected()]
    fnamevar.set(fname)
    lnamevar.set(lname)
    phonevar.set(phone)


def make_window():
    global fnamevar, lnamevar, phonevar, select
    win = Tk()

    frame1 = Frame(win)
    frame1.pack()

    Label(frame1, text="First Name").grid(row=0, column=0, sticky=W)
    fnamevar = StringVar()
    fname = Entry(frame1, textvariable=fnamevar)
    fname.grid(row=0, column=1, sticky=W)

    Label(frame1, text="Last Name").grid(row=1, column=0, sticky=W)
    lnamevar = StringVar()
    lname = Entry(frame1, textvariable=lnamevar)
    lname.grid(row=1, column=1, sticky=W)

    Label(frame1, text="Phone").grid(row=2, column=0, sticky=W)
    phonevar = StringVar()
    phone = Entry(frame1, textvariable=phonevar)
    phone.grid(row=2, column=1, sticky=W)

    frame2 = Frame(win)       # Row of buttons
    frame2.pack()
    b1 = Button(frame2, text=" Add  ", command=add_entry)
    b2 = Button(frame2, text="Update", command=update_entry)
    b3 = Button(frame2, text="Delete", command=delete_entry)
    b4 = Button(frame2, text="Load  ", command=load_entry)
    b5 = Button(frame2, text="Refresh", command=set_select)
    b1.pack(side=LEFT)
    b2.pack(side=LEFT)
    b3.pack(side=LEFT)
    b4.pack(side=LEFT)
    b5.pack(side=LEFT)

    frame3 = Frame(win)       # select of names
    frame3.pack()
    scroll = Scrollbar(frame3, orient=VERTICAL)
    select = Listbox(frame3, yscrollcommand=scroll.set, height=6)
    scroll.config(command=select.yview)
    scroll.pack(side=RIGHT, fill=Y)
    select.pack(side=LEFT, fill=BOTH, expand=1)
    return win


def set_select():
    global phone_list
    cur.execute("SELECT id, fname, lname, phone FROM phones ORDER BY lname")
    phone_list = cur.fetchall()
    select.delete(0, END)
    for theid, fname, lname, phone in phone_list:
        select.insert(END, "{0}, {1}".format(lname, fname))


win = make_window()
set_select()
win.mainloop()
