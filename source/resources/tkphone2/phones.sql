DROP TABLE phones;
CREATE TABLE phones (
    id INT,
    fname VARCHAR(15),
    lname VARCHAR(25),
    phone VARCHAR(12)
);
INSERT INTO phones VALUES (1, 'Chris', 'Meyers', '301-343-4349');
INSERT INTO phones VALUES (2, 'Robert', 'Smith', '703-689-1234');
INSERT INTO phones VALUES (3, 'Janet', 'Jones', '240-483-5432');
INSERT INTO phones VALUES (4, 'Ralph', 'Barnhart', '856-683-2341');
INSERT INTO phones VALUES (5, 'Eric', 'Nelson', '609-485-2689');
INSERT INTO phones VALUES (6, 'Ford', 'Prefect', '571-987-6543');
INSERT INTO phones VALUES (7, 'Mary', 'Zigler', '202-567-8901');
INSERT INTO phones VALUES (8, 'Bob', 'Smith', '301-689-1234');
