#
#  A n i m a l . p y
#


class Node:
    "Node objects have a question, and  left and right pointer to other Nodes"
    def __init__(self, question, left=None, right=None):
        self.question = question
        self.left = left
        self.right = right


def yes(ques):
    "The user answers 'yes' or something similar. Otherwise it's a no"
    ans = input(ques)
    return ans[0].lower() == 'y'


knowledge = Node("bird")


def main():
    "Guess the animal. Add a new Node for a wrong guess."

    while yes("\nAre you thinking of an animal? "):
        p = knowledge
        while p.left is not None:
            p = p.right if yes(p.question+"? ") else p.left

        if yes("Is it a " + p.question + "? "):
            continue
        animal = input("What is the animal's name? ")
        question = input(f"What question would distinguish a {animal} "
                         f"from a {p.question}? ")
        p.left = Node(p.question)
        p.right = Node(animal)
        p.question = question

        if not yes(f"If the animal were {animal} the answer would be? "):
            p.right, p.left = p.left, p.right


if __name__ == "__main__":
    main()
